﻿using Gestión_colisiones.DAL;
using Gestión_colisiones.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestión_colisiones.BOL
{
    class UsuarioBOL
    {
        public UsuarioE login(UsuarioE u)
        {
            if (u.cedula.ToString().Equals(""))
            {
                MessageBox.Show("Cedula es requerida");
            }
            if (u.contrasena.Equals(""))
            {
                MessageBox.Show("Contraseña requerida");
            }
            return new UsuarioDAL().Login(u);

        }

    }
}
