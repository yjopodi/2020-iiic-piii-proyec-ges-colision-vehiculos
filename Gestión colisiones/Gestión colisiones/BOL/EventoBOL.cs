﻿using Gestión_colisiones.DAL;
using Gestión_colisiones.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestión_colisiones.BOL
{
    class EventoBOL
    {
        public EventoE Agregar(EventoE ec)
        {
            return new EventoDAL().AgregarDatos(ec);
        }
        public void eliminarEvento(string codigo) {
            new EventoDAL().EliminaEventoXml(codigo);
        }
        public EventoE editarEventoO(EventoE ee) {
            if (ee.codigo == null)
            {
                MessageBox.Show("Selecione el evento");
            }
            else
            {
                if (ee.numparte.Equals(""))
                {
                    MessageBox.Show("Lo siento debe ingresar el numero de parte");
                }
                else
                {
                    new EventoDAL().modificarEvento(ee);
                }
                
            }
            return null;

        }
        public EventoE editarEventoJ(EventoE ee)
        {
            if (ee.codigo == null)
            {
                MessageBox.Show("Selecione el evento");
            }
            else {
                if (ee.registro.Equals(""))
                {
                    MessageBox.Show("Lo siento debe ingresar el numero de registro");
                }
                else
                {
                    new EventoDAL().modificarEventoJ(ee);
                }
            }
            return null;

        }
    }
}
