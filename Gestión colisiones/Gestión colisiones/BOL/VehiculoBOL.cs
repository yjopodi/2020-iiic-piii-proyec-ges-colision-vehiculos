﻿using Gestión_colisiones.DAL;
using Gestión_colisiones.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestión_colisiones.BOL
{
    class VehiculoBOL
    {
        public double Multa(VehiculoE v)
        {
            double multa = 0;
            if (v.tipo.Equals("Moto")) {
                multa += 10000 + ((15 * 10000)/100);
                if (v.anno < 2000)
                {
                    multa += (10 * multa / 100);
                }
            }
            if (v.tipo.Equals("Automóvil"))
            {
                multa += 25000 + ((30 * 25000) / 100);
                if (v.anno < 2000)
                {
                    multa += (10 * multa / 100);
                }
            }
            if (v.tipo.Equals("Bus"))
            {
                multa += 45000 + ((45 * 45000) / 100);
                if (v.anno < 2000)
                {
                    multa += (10 * multa / 100);
                }
            }
            if (v.tipo.Equals("Camión"))
            {
                multa +=65000 + ((70 * 65000) / 100);
                if (v.anno < 2000)
                {
                    multa += (10 * multa / 100);
                }
            }
            return multa;
        }
        public string CrearCodigo()
        {
            string caracteres = "ABCDEFGHIJQLMNOPQRSTUWXYZ0123456789";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            for (int i = 0; i < 9; i++)
            {
                res.Append(caracteres[rnd.Next(caracteres.Length)]);
            }
            return res.ToString();

        }
        
    }
}
