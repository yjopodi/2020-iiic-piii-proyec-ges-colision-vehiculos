﻿using Gestión_colisiones.Entities;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Gestión_colisiones.DAL
{
    class UsuarioDAL
    {
        XmlDocument doc;
        string rutaX = "Datos.xml";

        public void CrearXML(string nodoRaiz) {

            doc = new XmlDocument();
            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", "no");

            XmlNode nodo = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, nodo);

            XmlNode elemento = doc.CreateElement(nodoRaiz);
            doc.AppendChild(elemento);

            doc.Save(rutaX);
        }

        public void AgregarDatos(UsuarioE u) {
            doc = new XmlDocument();
            doc.Load(rutaX);
            XmlNode usuario = CrearUsuario(u);
            XmlNode nodo = doc.DocumentElement;
            nodo.InsertAfter(usuario, nodo.LastChild);
            doc.Save(rutaX);
        
        }

        public XmlNode CrearUsuario(UsuarioE u) 
        {
            XmlNode usuario = doc.CreateElement("usuario");

            XmlElement xcedula = doc.CreateElement("cedula");
            xcedula.InnerText = u.cedula.ToString();
            usuario.AppendChild(xcedula);

            
            XmlElement xcontraseña = doc.CreateElement("contraseña");
            xcontraseña.InnerText = u.contrasena;
            usuario.AppendChild(xcontraseña);

            XmlElement xnombre = doc.CreateElement("nombre");
            xnombre.InnerText = u.nombre;
            usuario.AppendChild(xnombre);

            XmlElement xtipo = doc.CreateElement("tipo");
            xtipo.InnerText = u.tipo;
            usuario.AppendChild(xtipo);

            return usuario;
        
        }
        public UsuarioE Login(UsuarioE u)
        {
            doc = new XmlDocument();
            doc.Load(rutaX);


            XmlNodeList listaUsuarios = doc.SelectNodes("Gestion_Colisiones/usuario");
            int i = 0;
            foreach (XmlNode item in listaUsuarios)
            {
                
                if (item.SelectSingleNode("cedula").InnerText.Equals(u.cedula.ToString()))
                {
                    i--;
                    if (item.SelectSingleNode("contraseña").InnerText.Equals(u.contrasena))
                    {
                        u.nombre = item.SelectSingleNode("nombre").InnerText;
                        u.tipo = item.SelectSingleNode("tipo").InnerText;
                        break;
                    }
                    MessageBox.Show("Contraseña incorrecta");
                }
                else
                {
                    i++;
                }
                if (i == listaUsuarios.Count) {
                    MessageBox.Show("Usuario no registrado");
                }

            }
            doc.Save(rutaX);

            return null;
        }


    }
}
