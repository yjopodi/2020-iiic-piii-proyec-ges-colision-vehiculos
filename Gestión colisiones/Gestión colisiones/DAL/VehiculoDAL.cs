﻿using Gestión_colisiones.BOL;
using Gestión_colisiones.Entities;
using System;
using System.Collections.Generic;
using System.Xml;

namespace Gestión_colisiones.DAL
{
    class VehiculoDAL
    {
        XmlDocument doc;
        string rutaX = "Datos.xml";
        public void AgregarDatos(VehiculoE v)
        {
            doc = new XmlDocument();
            doc.Load(rutaX);
            XmlNode vehiculo = CrearVehiculo(v);
            XmlNode nodo = doc.DocumentElement;
            nodo.InsertAfter(vehiculo, nodo.LastChild);
            doc.Save(rutaX);

        }

        public XmlNode CrearVehiculo(VehiculoE v)
        {
            XmlNode vehiculo = doc.CreateElement("vehiculo");

            XmlElement xplaca = doc.CreateElement("placa");
            xplaca.InnerText = v.placa.ToString();
            vehiculo.AppendChild(xplaca);


            XmlElement xanno = doc.CreateElement("contraseña");
            xanno.InnerText = v.anno.ToString();
            vehiculo.AppendChild(xanno);

            XmlElement xmarca = doc.CreateElement("marca");
            xmarca.InnerText = v.marca;
            vehiculo.AppendChild(xmarca);

            XmlElement xcolor = doc.CreateElement("color");
            xcolor.InnerText = v.color;
            vehiculo.AppendChild(xcolor);

            XmlElement xtipo = doc.CreateElement("tipo");
            xtipo.InnerText = v.tipo;
            vehiculo.AppendChild(xtipo);

            return vehiculo;

        }
        public void EliminaVehiculoXml(string placa)
        {
            doc = new XmlDocument();
            doc.Load(rutaX);

            XmlNodeList listaVehiculos = doc.SelectNodes("Gestion_Colisiones/vehiculo");

            XmlNode borrado = doc.DocumentElement;

            foreach (XmlNode item in listaVehiculos)
            {
                if (item.SelectSingleNode("placa").InnerText == placa)
                {
                    XmlNode borrar = item;
                    borrado.RemoveChild(borrar);

                }

            }
            doc.Save(rutaX);
        }

        public List<VehiculoE> LeerPVehiculosXml()
        {
            doc = new XmlDocument();
            doc.Load(rutaX);

            XmlNodeList listaVehiculos = doc.SelectNodes("Gestion_Colisiones/vehiculo");
            List<VehiculoE> vehiculos = new List<VehiculoE>();

            foreach (XmlNode item in listaVehiculos)
            {

                VehiculoE v = new VehiculoE();
                v.placa = Convert.ToInt32(item.SelectSingleNode("placa").InnerText);
                vehiculos.Add(v);

            }
            return vehiculos;
        }
        public VehiculoE LeerVehiculoXml(int placa)
        {
            VehiculoE v = new VehiculoE();
            VehiculoBOL vb = new VehiculoBOL();
            doc = new XmlDocument();
            doc.Load(rutaX);

            XmlNodeList listaVehiculos = doc.SelectNodes("Gestion_Colisiones/vehiculo");

            foreach (XmlNode item in listaVehiculos)
            {
                if (item.SelectSingleNode("placa").InnerText.Equals(placa.ToString())) { 
                v.placa = placa;
                v.anno = Convert.ToInt32(item.SelectSingleNode("contraseña").InnerText);
                v.marca = item.SelectSingleNode("marca").InnerText;
                v.color = item.SelectSingleNode("color").InnerText;
                v.tipo = item.SelectSingleNode("tipo").InnerText;
            }
            }
            return v;
        }
        public List<VehiculoE> LLenarDataG()
        {

            doc = new XmlDocument();
            doc.Load(rutaX);

            XmlNodeList listaVehiculos = doc.SelectNodes("Gestion_Colisiones/vehiculo");
            List<VehiculoE> vehiculos = new List<VehiculoE>();

            foreach (XmlNode item in listaVehiculos)
            {

                VehiculoE v = new VehiculoE();
                v.placa = Convert.ToInt32(item.SelectSingleNode("placa").InnerText);
                v.anno = Convert.ToInt32(item.SelectSingleNode("contraseña").InnerText);
                v.marca = item.SelectSingleNode("marca").InnerText;
                v.color = item.SelectSingleNode("color").InnerText;
                v.tipo = item.SelectSingleNode("tipo").InnerText;
                vehiculos.Add(v);
            }
            return vehiculos;


        }

    }
}
