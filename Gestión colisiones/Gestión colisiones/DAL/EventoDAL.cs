﻿using Gestión_colisiones.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Gestión_colisiones.DAL
{
    class EventoDAL
    {
        XmlDocument doc;
        string rutaX = "Datos.xml";
        public EventoE AgregarDatos(EventoE ec)
        {
            doc = new XmlDocument();
            doc.Load(rutaX);
            XmlNode evento = CrearEvento(ec);
            XmlNode nodo = doc.DocumentElement;
            nodo.InsertAfter(evento, nodo.LastChild);
            doc.Save(rutaX);
            return null;

        }

        public XmlNode CrearEvento(EventoE ec)
        {
            XmlNode evento = doc.CreateElement("Evento");

            XmlElement xcodigo = doc.CreateElement("codigo");
            xcodigo.InnerText = ec.codigo.Trim();
            evento.AppendChild(xcodigo);

            XmlElement xcciudadano = doc.CreateElement("cedciudadano");
            xcciudadano.InnerText = ec.cedCiuda.ToString().Trim();
            evento.AppendChild(xcciudadano);

            XmlElement xnombrec = doc.CreateElement("nombrec");
            xnombrec.InnerText = ec.nombreC.Trim();
            evento.AppendChild(xnombrec);

            XmlElement xplaca = doc.CreateElement("placa");
            xplaca.InnerText = ec.placa.ToString().Trim();
            evento.AppendChild(xplaca);

            XmlElement xvehiculo = doc.CreateElement("vehiculo");
            xvehiculo.InnerText = ec.vehiculo.Trim();
            evento.AppendChild(xvehiculo);

            XmlElement xprovincia = doc.CreateElement("provincia");
            xprovincia.InnerText = ec.provincia.Trim();
            evento.AppendChild(xprovincia);

            XmlElement xfecha = doc.CreateElement("fecha");
            xfecha.InnerText = ec.fecha.ToString().Trim();
            evento.AppendChild(xfecha);

            XmlElement xmulta = doc.CreateElement("multa");
            xmulta.InnerText = ec.multa.ToString().Trim();
            evento.AppendChild(xmulta);

            XmlElement xcedoficial = doc.CreateElement("cedoficial");
            xcedoficial.InnerText = ec.cedOficial.ToString().Trim();
            evento.AppendChild(xcedoficial);

            XmlElement xnombreo = doc.CreateElement("nombreoficial");
            xnombreo.InnerText = ec.nombreOficial.Trim();
            evento.AppendChild(xnombreo);

            XmlElement xnumparte = doc.CreateElement("parte");
            xnumparte.InnerText = ec.numparte.ToString().Trim();
            evento.AppendChild(xnumparte);

            XmlElement xestado = doc.CreateElement("estado");
            xestado.InnerText = ec.estado.Trim();
            evento.AppendChild(xestado);

            XmlElement xcedjuez = doc.CreateElement("cedjuez");
            xcedjuez.InnerText = ec.cedJuez.ToString().Trim();
            evento.AppendChild(xcedjuez);

            XmlElement xnombjuez = doc.CreateElement("nombreJuez");
            xnombjuez.InnerText = ec.nombreJuez.Trim();
            evento.AppendChild(xnombjuez);

            XmlElement xregistro = doc.CreateElement("registro");
            xregistro.InnerText = ec.registro.ToString().Trim();
            evento.AppendChild(xregistro);
            return evento;

        }
        public void EliminaEventoXml(string codigo)
        {
            doc = new XmlDocument();
            doc.Load(rutaX);

            XmlNodeList listaEventos = doc.SelectNodes("Gestion_Colisiones/Evento");

            XmlNode borrado = doc.DocumentElement;

            foreach (XmlNode item in listaEventos)
            {
                if (item.SelectSingleNode("codigo").InnerText == codigo)
                {
                    XmlNode borrar = item;
                    borrado.RemoveChild(borrar);

                }

            }
            doc.Save(rutaX);
        }
        public List<EventoE> LLenarDataG()
        {

            doc = new XmlDocument();
            doc.Load(rutaX);

            XmlNodeList listaEventos = doc.SelectNodes("Gestion_Colisiones/Evento");
            List<EventoE> eventos = new List<EventoE>();

            foreach (XmlNode item in listaEventos)
            {
                
                    EventoE ee = new EventoE();
                    ee.codigo = item.SelectSingleNode("codigo").InnerText.Trim();
                    ee.cedCiuda = Convert.ToInt32(item.SelectSingleNode("cedciudadano").InnerText.Trim());
                    ee.nombreC = item.SelectSingleNode("nombrec").InnerText.Trim();
                    ee.placa = Convert.ToInt32(item.SelectSingleNode("placa").InnerText.Trim());
                    ee.vehiculo = item.SelectSingleNode("vehiculo").InnerText.Trim();
                    ee.provincia = item.SelectSingleNode("provincia").InnerText.Trim();
                    ee.fecha = Convert.ToDateTime(item.SelectSingleNode("fecha").InnerText.Trim());
                    ee.multa = Convert.ToInt32(item.SelectSingleNode("multa").InnerText.Trim());
                    ee.estado = item.SelectSingleNode("estado").InnerText.Trim();
                    ee.cedOficial = Convert.ToInt32(item.SelectSingleNode("cedoficial").InnerText.Trim());
                    ee.nombreOficial = item.SelectSingleNode("nombreoficial").InnerText;
                    ee.numparte = Convert.ToInt32(item.SelectSingleNode("parte").InnerText);
                    eventos.Add(ee);
            }
            return eventos;


        }
        public EventoE modificarEvento(EventoE e) {
            doc = new XmlDocument();
            doc.Load(rutaX);

            XmlNodeList listaEventos = doc.SelectNodes("Gestion_Colisiones/Evento");
            foreach (XmlNode item in listaEventos) {
                if (item.SelectSingleNode("parte").InnerText.Equals(e.numparte.ToString()))
                {
                    MessageBox.Show("Numero de parte previamente registrado");
                    break;
                }
                else{
                    if (item.FirstChild.InnerText == e.codigo.ToString())
                    {
                        item.SelectSingleNode("fecha").InnerText = e.fecha.ToString();
                        item.SelectSingleNode("estado").InnerText = e.estado;
                        item.SelectSingleNode("parte").InnerText = e.numparte.ToString();
                        item.SelectSingleNode("cedoficial").InnerText = e.cedOficial.ToString();
                        item.SelectSingleNode("nombreoficial").InnerText = e.nombreOficial;
                    }
                }
            
            }
            doc.Save(rutaX);
            return null;
        }
        public void modificarEventoJ(EventoE ee)
        {
            doc = new XmlDocument();
            doc.Load(rutaX);

            XmlNodeList listaEventos = doc.SelectNodes("Gestion_Colisiones/Evento");
            foreach (XmlNode item in listaEventos)
            {
                if (item.SelectSingleNode("registro").InnerText.Equals(ee.registro.ToString()))
                {
                    MessageBox.Show("Numero de registro previamente registrado");
                    break;
                }
                else
                {
                    if (item.FirstChild.InnerText.Equals(ee.codigo.ToString()))
                    {

                        item.SelectSingleNode("fecha").InnerText = ee.fecha.ToString();
                        item.SelectSingleNode("estado").InnerText = ee.estado.ToString();
                        item.SelectSingleNode("registro").InnerText = ee.registro.ToString();
                        item.SelectSingleNode("cedjuez").InnerText = ee.cedJuez.ToString();
                        item.SelectSingleNode("nombreJuez").InnerText = ee.nombreJuez.ToString();
                    }
                }

            }
            doc.Save(rutaX);
           
        }

    }
}
