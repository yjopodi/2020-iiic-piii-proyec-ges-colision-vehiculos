﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestión_colisiones.Entities
{
    class EventoE
    {
        public string codigo { get; set; }
        public int cedCiuda { get; set; }
        public string nombreC { get; set; }
        public int placa { get; set; }
        public string vehiculo { get; set; }
        public string provincia { get; set; }
        public DateTime fecha { get; set; }
        public double multa { get; set; }
        public int cedOficial { get; set; }
        public string nombreOficial { get; set; }
        public int numparte { get; set; }
        public string estado { get; set; }
        public int cedJuez { get; set; }
        public string nombreJuez { get; set; }
        public int registro { get; set; }
    }
}
