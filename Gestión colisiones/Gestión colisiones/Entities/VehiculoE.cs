﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestión_colisiones.Entities
{
    class VehiculoE
    {
        public int placa { set; get; }
        public int anno { set; get; }
        public string marca { set; get; }
        public string color { set; get; }
        public string tipo { set; get; }
        public override string ToString()
        {
            return ""+ placa + "" + anno + "" + marca + "" + color + "" + tipo + "";
        }
        
    }
}
