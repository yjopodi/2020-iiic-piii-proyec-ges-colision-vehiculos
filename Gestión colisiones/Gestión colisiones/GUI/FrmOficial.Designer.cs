﻿namespace Gestión_colisiones.GUI
{
    partial class FrmOficial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOficial));
            this.lblCerrarSesión = new System.Windows.Forms.Label();
            this.txtNumParte = new System.Windows.Forms.TextBox();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.dtgPorComprobar = new System.Windows.Forms.DataGridView();
            this.txtCodigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPlaca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtVehiculo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPropietario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtCedPropie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtLugar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtFechaH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtMulta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtEstado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblOficial = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblfechaH = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dtgPorComprobar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCerrarSesión
            // 
            this.lblCerrarSesión.AutoSize = true;
            this.lblCerrarSesión.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.6F);
            this.lblCerrarSesión.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblCerrarSesión.Image = ((System.Drawing.Image)(resources.GetObject("lblCerrarSesión.Image")));
            this.lblCerrarSesión.Location = new System.Drawing.Point(1383, 0);
            this.lblCerrarSesión.MaximumSize = new System.Drawing.Size(60, 60);
            this.lblCerrarSesión.MinimumSize = new System.Drawing.Size(70, 60);
            this.lblCerrarSesión.Name = "lblCerrarSesión";
            this.lblCerrarSesión.Size = new System.Drawing.Size(70, 60);
            this.lblCerrarSesión.TabIndex = 0;
            this.lblCerrarSesión.Text = "Cerrar sesión";
            this.lblCerrarSesión.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.lblCerrarSesión.Click += new System.EventHandler(this.lblCerrarSesión_Click);
            // 
            // txtNumParte
            // 
            this.txtNumParte.Location = new System.Drawing.Point(33, 146);
            this.txtNumParte.MinimumSize = new System.Drawing.Size(32, 32);
            this.txtNumParte.Name = "txtNumParte";
            this.txtNumParte.Size = new System.Drawing.Size(195, 32);
            this.txtNumParte.TabIndex = 1;
            this.txtNumParte.TextChanged += new System.EventHandler(this.txtNumParte_TextChanged);
            this.txtNumParte.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumParte_KeyPress);
            // 
            // btnAgregar
            // 
            this.btnAgregar.BackColor = System.Drawing.Color.Green;
            this.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAgregar.ForeColor = System.Drawing.SystemColors.Control;
            this.btnAgregar.Location = new System.Drawing.Point(234, 146);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(71, 32);
            this.btnAgregar.TabIndex = 2;
            this.btnAgregar.Text = "Asignar";
            this.btnAgregar.UseVisualStyleBackColor = false;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // dtgPorComprobar
            // 
            this.dtgPorComprobar.AllowUserToAddRows = false;
            this.dtgPorComprobar.AllowUserToDeleteRows = false;
            this.dtgPorComprobar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPorComprobar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtCodigo,
            this.txtPlaca,
            this.txtVehiculo,
            this.txtPropietario,
            this.txtCedPropie,
            this.txtLugar,
            this.txtFechaH,
            this.txtMulta,
            this.txtEstado});
            this.dtgPorComprobar.Location = new System.Drawing.Point(33, 195);
            this.dtgPorComprobar.Name = "dtgPorComprobar";
            this.dtgPorComprobar.ReadOnly = true;
            this.dtgPorComprobar.RowHeadersWidth = 51;
            this.dtgPorComprobar.RowTemplate.Height = 24;
            this.dtgPorComprobar.Size = new System.Drawing.Size(1389, 519);
            this.dtgPorComprobar.TabIndex = 3;
            this.dtgPorComprobar.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgPorComprobar_CellClick);
            // 
            // txtCodigo
            // 
            this.txtCodigo.HeaderText = "Código";
            this.txtCodigo.MinimumWidth = 6;
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.ReadOnly = true;
            this.txtCodigo.Width = 125;
            // 
            // txtPlaca
            // 
            this.txtPlaca.HeaderText = "Placa";
            this.txtPlaca.MinimumWidth = 6;
            this.txtPlaca.Name = "txtPlaca";
            this.txtPlaca.ReadOnly = true;
            this.txtPlaca.Width = 125;
            // 
            // txtVehiculo
            // 
            this.txtVehiculo.HeaderText = "Vehiculo";
            this.txtVehiculo.MinimumWidth = 6;
            this.txtVehiculo.Name = "txtVehiculo";
            this.txtVehiculo.ReadOnly = true;
            this.txtVehiculo.Width = 125;
            // 
            // txtPropietario
            // 
            this.txtPropietario.HeaderText = "Propietario";
            this.txtPropietario.MinimumWidth = 6;
            this.txtPropietario.Name = "txtPropietario";
            this.txtPropietario.ReadOnly = true;
            this.txtPropietario.Width = 125;
            // 
            // txtCedPropie
            // 
            this.txtCedPropie.HeaderText = "Ced.Propietario";
            this.txtCedPropie.MinimumWidth = 6;
            this.txtCedPropie.Name = "txtCedPropie";
            this.txtCedPropie.ReadOnly = true;
            this.txtCedPropie.Width = 125;
            // 
            // txtLugar
            // 
            this.txtLugar.HeaderText = "Lugar";
            this.txtLugar.MinimumWidth = 6;
            this.txtLugar.Name = "txtLugar";
            this.txtLugar.ReadOnly = true;
            this.txtLugar.Width = 125;
            // 
            // txtFechaH
            // 
            this.txtFechaH.HeaderText = "Fecha y hora";
            this.txtFechaH.MinimumWidth = 6;
            this.txtFechaH.Name = "txtFechaH";
            this.txtFechaH.ReadOnly = true;
            this.txtFechaH.Width = 125;
            // 
            // txtMulta
            // 
            this.txtMulta.HeaderText = "Multa";
            this.txtMulta.MinimumWidth = 6;
            this.txtMulta.Name = "txtMulta";
            this.txtMulta.ReadOnly = true;
            this.txtMulta.Width = 125;
            // 
            // txtEstado
            // 
            this.txtEstado.HeaderText = "Estado";
            this.txtEstado.MinimumWidth = 6;
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.ReadOnly = true;
            this.txtEstado.Width = 125;
            // 
            // lblOficial
            // 
            this.lblOficial.AutoSize = true;
            this.lblOficial.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOficial.ForeColor = System.Drawing.SystemColors.Control;
            this.lblOficial.Location = new System.Drawing.Point(29, 717);
            this.lblOficial.Name = "lblOficial";
            this.lblOficial.Size = new System.Drawing.Size(60, 19);
            this.lblOficial.TabIndex = 4;
            this.lblOficial.Text = "Oficial:";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.9F);
            this.lblNombre.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblNombre.Location = new System.Drawing.Point(86, 717);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(68, 20);
            this.lblNombre.TabIndex = 5;
            this.lblNombre.Text = "Nombre";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 30.2F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label1.Location = new System.Drawing.Point(636, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 60);
            this.label1.TabIndex = 6;
            this.label1.Text = "Partes";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(849, 57);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(61, 59);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // lblfechaH
            // 
            this.lblfechaH.AutoSize = true;
            this.lblfechaH.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfechaH.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.lblfechaH.Location = new System.Drawing.Point(1302, 721);
            this.lblfechaH.Name = "lblfechaH";
            this.lblfechaH.Size = new System.Drawing.Size(92, 16);
            this.lblfechaH.TabIndex = 11;
            this.lblfechaH.Text = "Fecha y hora";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FrmOficial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(1453, 745);
            this.Controls.Add(this.lblfechaH);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.lblOficial);
            this.Controls.Add(this.dtgPorComprobar);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.txtNumParte);
            this.Controls.Add(this.lblCerrarSesión);
            this.Name = "FrmOficial";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmOficial";
            ((System.ComponentModel.ISupportInitialize)(this.dtgPorComprobar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCerrarSesión;
        private System.Windows.Forms.TextBox txtNumParte;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.DataGridView dtgPorComprobar;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtCodigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPlaca;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtVehiculo;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPropietario;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtCedPropie;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtLugar;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtFechaH;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtMulta;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtEstado;
        private System.Windows.Forms.Label lblOficial;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblfechaH;
        private System.Windows.Forms.Timer timer1;
    }
}