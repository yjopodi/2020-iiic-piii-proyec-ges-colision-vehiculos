﻿namespace Gestión_colisiones.GUI
{
    partial class FrmCiudadano
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCiudadano));
            this.lblCerrarSesión = new System.Windows.Forms.Label();
            this.cbxPlaca = new System.Windows.Forms.ComboBox();
            this.cbxProvincias = new System.Windows.Forms.ComboBox();
            this.btnCrearEvento = new System.Windows.Forms.Button();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblPlaca = new System.Windows.Forms.Label();
            this.lblLugar = new System.Windows.Forms.Label();
            this.lblfechH = new System.Windows.Forms.Label();
            this.tHoraFech = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtgPorComprobar = new System.Windows.Forms.DataGridView();
            this.txtCodigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPlaca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtVehiculo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPropietario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtCedPropie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtLugar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtFechaH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtMulta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtEstado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtEliminar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPorComprobar)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCerrarSesión
            // 
            this.lblCerrarSesión.AutoSize = true;
            this.lblCerrarSesión.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.6F);
            this.lblCerrarSesión.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblCerrarSesión.Image = ((System.Drawing.Image)(resources.GetObject("lblCerrarSesión.Image")));
            this.lblCerrarSesión.Location = new System.Drawing.Point(1393, -13);
            this.lblCerrarSesión.MaximumSize = new System.Drawing.Size(52, 80);
            this.lblCerrarSesión.MinimumSize = new System.Drawing.Size(61, 80);
            this.lblCerrarSesión.Name = "lblCerrarSesión";
            this.lblCerrarSesión.Size = new System.Drawing.Size(61, 80);
            this.lblCerrarSesión.TabIndex = 1;
            this.lblCerrarSesión.Text = "Cerrar sesión";
            this.lblCerrarSesión.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.lblCerrarSesión.Click += new System.EventHandler(this.lblCerrarSesión_Click);
            // 
            // cbxPlaca
            // 
            this.cbxPlaca.BackColor = System.Drawing.SystemColors.Control;
            this.cbxPlaca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxPlaca.Font = new System.Drawing.Font("Arial", 10.1F);
            this.cbxPlaca.FormattingEnabled = true;
            this.cbxPlaca.Location = new System.Drawing.Point(204, 242);
            this.cbxPlaca.Name = "cbxPlaca";
            this.cbxPlaca.Size = new System.Drawing.Size(146, 27);
            this.cbxPlaca.TabIndex = 2;
            // 
            // cbxProvincias
            // 
            this.cbxProvincias.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxProvincias.Font = new System.Drawing.Font("Arial", 10.1F);
            this.cbxProvincias.ForeColor = System.Drawing.Color.Black;
            this.cbxProvincias.FormattingEnabled = true;
            this.cbxProvincias.Location = new System.Drawing.Point(471, 239);
            this.cbxProvincias.Name = "cbxProvincias";
            this.cbxProvincias.Size = new System.Drawing.Size(146, 27);
            this.cbxProvincias.TabIndex = 3;
            // 
            // btnCrearEvento
            // 
            this.btnCrearEvento.BackColor = System.Drawing.Color.ForestGreen;
            this.btnCrearEvento.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCrearEvento.ForeColor = System.Drawing.Color.Azure;
            this.btnCrearEvento.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCrearEvento.Location = new System.Drawing.Point(1115, 229);
            this.btnCrearEvento.Name = "btnCrearEvento";
            this.btnCrearEvento.Size = new System.Drawing.Size(100, 40);
            this.btnCrearEvento.TabIndex = 5;
            this.btnCrearEvento.Text = "Crear";
            this.btnCrearEvento.UseVisualStyleBackColor = false;
            this.btnCrearEvento.Click += new System.EventHandler(this.btnCrearEvento_Click_1);
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Arial", 10.1F);
            this.lblNombre.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblNombre.Location = new System.Drawing.Point(138, 653);
            this.lblNombre.MinimumSize = new System.Drawing.Size(60, 20);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(66, 20);
            this.lblNombre.TabIndex = 6;
            this.lblNombre.Text = "Nombre";
            // 
            // lblPlaca
            // 
            this.lblPlaca.AutoSize = true;
            this.lblPlaca.Font = new System.Drawing.Font("Arial", 10.1F);
            this.lblPlaca.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblPlaca.Location = new System.Drawing.Point(138, 249);
            this.lblPlaca.MinimumSize = new System.Drawing.Size(60, 20);
            this.lblPlaca.Name = "lblPlaca";
            this.lblPlaca.Size = new System.Drawing.Size(60, 20);
            this.lblPlaca.TabIndex = 7;
            this.lblPlaca.Text = "Placa#";
            // 
            // lblLugar
            // 
            this.lblLugar.AutoSize = true;
            this.lblLugar.Font = new System.Drawing.Font("Arial", 10.1F);
            this.lblLugar.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblLugar.Location = new System.Drawing.Point(411, 245);
            this.lblLugar.MinimumSize = new System.Drawing.Size(60, 20);
            this.lblLugar.Name = "lblLugar";
            this.lblLugar.Size = new System.Drawing.Size(60, 20);
            this.lblLugar.TabIndex = 8;
            this.lblLugar.Text = "Lugar";
            // 
            // lblfechH
            // 
            this.lblfechH.AutoSize = true;
            this.lblfechH.Font = new System.Drawing.Font("Arial", 8.8F);
            this.lblfechH.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lblfechH.Location = new System.Drawing.Point(1207, 653);
            this.lblfechH.MinimumSize = new System.Drawing.Size(60, 20);
            this.lblfechH.Name = "lblfechH";
            this.lblfechH.Size = new System.Drawing.Size(93, 20);
            this.lblfechH.TabIndex = 9;
            this.lblfechH.Text = "Fecha y hora";
            // 
            // tHoraFech
            // 
            this.tHoraFech.Enabled = true;
            this.tHoraFech.Interval = 1;
            this.tHoraFech.Tick += new System.EventHandler(this.tHoraFech_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(838, 69);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(65, 61);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 30.3F);
            this.label1.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label1.Location = new System.Drawing.Point(619, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 58);
            this.label1.TabIndex = 13;
            this.label1.Text = "Evento";
            // 
            // dtgPorComprobar
            // 
            this.dtgPorComprobar.AllowUserToAddRows = false;
            this.dtgPorComprobar.AllowUserToDeleteRows = false;
            this.dtgPorComprobar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPorComprobar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtCodigo,
            this.txtPlaca,
            this.txtVehiculo,
            this.txtPropietario,
            this.txtCedPropie,
            this.txtLugar,
            this.txtFechaH,
            this.txtMulta,
            this.txtEstado});
            this.dtgPorComprobar.Location = new System.Drawing.Point(142, 272);
            this.dtgPorComprobar.Name = "dtgPorComprobar";
            this.dtgPorComprobar.ReadOnly = true;
            this.dtgPorComprobar.RowHeadersWidth = 51;
            this.dtgPorComprobar.RowTemplate.Height = 24;
            this.dtgPorComprobar.Size = new System.Drawing.Size(1179, 367);
            this.dtgPorComprobar.TabIndex = 14;
            this.dtgPorComprobar.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgPorComprobar_CellClick);
            // 
            // txtCodigo
            // 
            this.txtCodigo.HeaderText = "Código";
            this.txtCodigo.MinimumWidth = 6;
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.ReadOnly = true;
            this.txtCodigo.Width = 125;
            // 
            // txtPlaca
            // 
            this.txtPlaca.HeaderText = "Placa";
            this.txtPlaca.MinimumWidth = 6;
            this.txtPlaca.Name = "txtPlaca";
            this.txtPlaca.ReadOnly = true;
            this.txtPlaca.Width = 125;
            // 
            // txtVehiculo
            // 
            this.txtVehiculo.HeaderText = "Vehiculo";
            this.txtVehiculo.MinimumWidth = 6;
            this.txtVehiculo.Name = "txtVehiculo";
            this.txtVehiculo.ReadOnly = true;
            this.txtVehiculo.Width = 125;
            // 
            // txtPropietario
            // 
            this.txtPropietario.HeaderText = "Propietario";
            this.txtPropietario.MinimumWidth = 6;
            this.txtPropietario.Name = "txtPropietario";
            this.txtPropietario.ReadOnly = true;
            this.txtPropietario.Width = 125;
            // 
            // txtCedPropie
            // 
            this.txtCedPropie.HeaderText = "Ced.Propietario";
            this.txtCedPropie.MinimumWidth = 6;
            this.txtCedPropie.Name = "txtCedPropie";
            this.txtCedPropie.ReadOnly = true;
            this.txtCedPropie.Width = 125;
            // 
            // txtLugar
            // 
            this.txtLugar.HeaderText = "Lugar";
            this.txtLugar.MinimumWidth = 6;
            this.txtLugar.Name = "txtLugar";
            this.txtLugar.ReadOnly = true;
            this.txtLugar.Width = 125;
            // 
            // txtFechaH
            // 
            this.txtFechaH.HeaderText = "Fecha y hora";
            this.txtFechaH.MinimumWidth = 6;
            this.txtFechaH.Name = "txtFechaH";
            this.txtFechaH.ReadOnly = true;
            this.txtFechaH.Width = 125;
            // 
            // txtMulta
            // 
            this.txtMulta.HeaderText = "Multa";
            this.txtMulta.MinimumWidth = 6;
            this.txtMulta.Name = "txtMulta";
            this.txtMulta.ReadOnly = true;
            this.txtMulta.Width = 125;
            // 
            // txtEstado
            // 
            this.txtEstado.HeaderText = "Estado";
            this.txtEstado.MinimumWidth = 6;
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.ReadOnly = true;
            this.txtEstado.Width = 125;
            // 
            // txtEliminar
            // 
            this.txtEliminar.BackColor = System.Drawing.Color.ForestGreen;
            this.txtEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.txtEliminar.ForeColor = System.Drawing.Color.Azure;
            this.txtEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.txtEliminar.Location = new System.Drawing.Point(1221, 229);
            this.txtEliminar.Name = "txtEliminar";
            this.txtEliminar.Size = new System.Drawing.Size(100, 40);
            this.txtEliminar.TabIndex = 15;
            this.txtEliminar.Text = "Eliminar";
            this.txtEliminar.UseVisualStyleBackColor = false;
            this.txtEliminar.Click += new System.EventHandler(this.txtEliminar_Click);
            // 
            // FrmCiudadano
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(1453, 745);
            this.Controls.Add(this.txtEliminar);
            this.Controls.Add(this.dtgPorComprobar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblfechH);
            this.Controls.Add(this.lblLugar);
            this.Controls.Add(this.lblPlaca);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.btnCrearEvento);
            this.Controls.Add(this.cbxProvincias);
            this.Controls.Add(this.cbxPlaca);
            this.Controls.Add(this.lblCerrarSesión);
            this.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmCiudadano";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reporte";
            this.Load += new System.EventHandler(this.FrmCiudadano_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPorComprobar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCerrarSesión;
        private System.Windows.Forms.ComboBox cbxPlaca;
        private System.Windows.Forms.ComboBox cbxProvincias;
        private System.Windows.Forms.Button btnCrearEvento;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblPlaca;
        private System.Windows.Forms.Label lblLugar;
        private System.Windows.Forms.Label lblfechH;
        private System.Windows.Forms.Timer tHoraFech;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dtgPorComprobar;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtCodigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPlaca;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtVehiculo;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPropietario;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtCedPropie;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtLugar;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtFechaH;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtMulta;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtEstado;
        private System.Windows.Forms.Button txtEliminar;
    }
}