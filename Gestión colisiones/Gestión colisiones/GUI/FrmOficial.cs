﻿using Gestión_colisiones.BOL;
using Gestión_colisiones.DAL;
using Gestión_colisiones.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestión_colisiones.GUI
{
    public partial class FrmOficial : Form
    {
        FrmLogin frm = new FrmLogin();
        int c;
        EventoE ee = new EventoE();
        EventoBOL eb = new EventoBOL();
        
        public FrmOficial(string nombre, int cedula)
        {
            InitializeComponent();
            lblNombre.Text = nombre;
            c = cedula;
            mostrar();
        }

        private void lblCerrarSesión_Click(object sender, EventArgs e)
        {
            Close();
            frm.Show();
            Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }
        private void mostrar()
        {

            dtgPorComprobar.Rows.Clear();
            foreach (EventoE e in new EventoDAL().LLenarDataG())
            {
                if (e.estado.Equals("Abierto"))
                {
                    
                    dtgPorComprobar.Rows.Add(e.codigo, e.placa, e.vehiculo, e.nombreC, e.cedCiuda, e.provincia, e.fecha, e.multa, e.estado);
                }
            }

        }

        private void dtgPorComprobar_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dtgPorComprobar.Rows[e.RowIndex];
            ee.codigo = row.Cells[0].Value.ToString();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {

            if (txtNumParte.Text.Trim().Equals("")) {
                MessageBox.Show("Lo siento debe ingresar un numero de parte");
            }
            else
            {
                
                ee.numparte = Convert.ToInt32(txtNumParte.Text);
                ee.estado = "Por Aprobar";
                ee.cedOficial = c;
                ee.nombreOficial = lblNombre.Text;
                ee.fecha = DateTime.Now;
                eb.editarEventoO(ee);
                mostrar();
            }
        }
        private void soloNumeros(KeyPressEventArgs e) {
            if (Char.IsNumber(e.KeyChar))
            {
                e.Handled = false;

            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }
        
        
        }

        private void txtNumParte_KeyPress(object sender, KeyPressEventArgs e)
        {
            soloNumeros(e);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblfechaH.Text = DateTime.Now.ToString();
        }

        private void txtNumParte_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
