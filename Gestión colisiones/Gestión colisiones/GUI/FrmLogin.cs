﻿using Gestión_colisiones.BOL;
using Gestión_colisiones.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestión_colisiones.GUI
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void tipoU(UsuarioE u)
        {
            if (u.tipo.Equals("Ciudadano"))
            {
                FrmCiudadano fc = new FrmCiudadano(u.nombre, u.cedula);
                fc.Show();
                Hide();
            }
            if (u.tipo.Equals("Oficial de Tránsito"))
            {
                FrmOficial fo = new FrmOficial(u.nombre, u.cedula);
                fo.Show();
                Hide();
            }
            if (u.tipo.Equals("Oficina de Juzgado"))
            {
                frmJuzgado fj = new frmJuzgado(u.nombre, u.cedula);
                fj.Show();
                Hide();

            }

        }

        private void soloNumeros(KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
            {
                e.Handled = false;

            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }


        }


        private void txtCedula_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            soloNumeros(e);
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            UsuarioE u = new UsuarioE();
            if (txtCedula.Text.Trim() == null || txtCedula.Text.Trim().Equals(""))
            {
                MessageBox.Show("Número de cedula es requerido");
            }
            else
            {
                if (txtContraseña.Text.Trim().Equals("") )
                {
                    MessageBox.Show("Contraseña es requerida");

                }
                else
                {
                    u.cedula = int.Parse(txtCedula.Text.Trim());
                    u.contrasena = txtContraseña.Text.Trim();
                    UsuarioBOL ub = new UsuarioBOL();
                    ub.login(u);
                        tipoU(u);
                    
                }
            }
            
        }

        private void lblSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
