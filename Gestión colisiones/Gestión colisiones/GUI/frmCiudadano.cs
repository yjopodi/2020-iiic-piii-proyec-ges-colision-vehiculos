﻿using Gestión_colisiones.BOL;
using Gestión_colisiones.DAL;
using Gestión_colisiones.Entities;
using Gestión_colisiones.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Schema;

namespace Gestión_colisiones.GUI
{
    public partial class FrmCiudadano : Form
    {
        FrmLogin frm = new FrmLogin();
        int c;
        VehiculoE ve = new VehiculoE();
        public FrmCiudadano(string nombre, int cedula)
        {
            InitializeComponent();
            VehiculoDAL vd = new VehiculoDAL();
            UsuarioE u = new UsuarioE();
            mostrar();
            cbxPlaca.DataSource = vd.LeerPVehiculosXml();
            Provincias();
            lblNombre.Text = (nombre);
            c = cedula;
        }

        private void lblCerrarSesión_Click(object sender, EventArgs e)
        {
            Close();
            frm.Show();
            Hide();
        }

        public void Provincias() {
            cbxProvincias.Items.Add("San Jóse");
            cbxProvincias.Items.Add("Alajuela");
            cbxProvincias.Items.Add("Heredia");
            cbxProvincias.Items.Add("Cartago");
            cbxProvincias.Items.Add("Guanacaste");
            cbxProvincias.Items.Add("Puntarenas");
            cbxProvincias.Items.Add("Limón");
        }



        private void btnCrearEvento_Click(object sender, EventArgs e)
        {
            
        }

        private void tHoraFech_Tick(object sender, EventArgs e)
        {
            lblfechH.Text = DateTime.Now.ToString();
        }

        private void btnCrearEvento_Click_1(object sender, EventArgs e)
        {
            EventoE ee = new EventoE();

            if (cargarDatos() == true) {
                MessageBox.Show("Lo siento su placa ya presenta un evento");
            }
            else
            {

                VehiculoDAL vd = new VehiculoDAL();
                VehiculoBOL vb = new VehiculoBOL();
                EventoBOL eb = new EventoBOL();
                ee.codigo = vb.CrearCodigo();
                ee.placa = Convert.ToInt32(cbxPlaca.Text.Remove(cbxPlaca.Text.Length - 1));
                ee.provincia = cbxProvincias.SelectedItem.ToString();
                ee.fecha = DateTime.Now;
                ee.multa = vb.Multa(vd.LeerVehiculoXml(Convert.ToInt32(cbxPlaca.Text.Remove(cbxPlaca.Text.Length-1))));
                ee.estado = "Abierto";
                ee.cedCiuda = c;
                ee.nombreC = lblNombre.Text;
                ee.vehiculo = vd.LeerVehiculoXml(Convert.ToInt32(cbxPlaca.Text.Remove(cbxPlaca.Text.Length - 1))).tipo;
                ee.cedOficial = 0;
                ee.nombreOficial = "Incompleto";
                ee.numparte = 0;
                ee.cedJuez = 0;
                ee.nombreJuez = "Incompleto";
                ee.registro = 0;
                eb.Agregar(ee);
                mostrar();
            }
            cargarDatos();
        }

        public void mostrar() {
            dtgPorComprobar.Rows.Clear();
            foreach (EventoE ee in new EventoDAL().LLenarDataG())
            {  
                dtgPorComprobar.Rows.Add(ee.codigo, ee.placa, ee.vehiculo, ee.nombreC, ee.cedCiuda, ee.provincia, ee.fecha, ee.multa, ee.estado);
            }
        
        }

        private void FrmCiudadano_Load(object sender, EventArgs e)
        {

        }

        private void dtgPorComprobar_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = dtgPorComprobar.Rows[e.RowIndex];
            ve.placa = Convert.ToInt32(row.Cells[1].Value.ToString());
            ve.tipo = row.Cells[8].Value.ToString();
            ve.marca = row.Cells[0].Value.ToString();
        }
        private bool cargarDatos() {
            bool esta = false;
            for (int i = 0; i < dtgPorComprobar.RowCount; i++) {
                ve.placa = int.Parse(dtgPorComprobar.Rows[i].Cells[1].Value.ToString());
                
                if(ve.placa == Convert.ToInt32(cbxPlaca.Text.Remove(cbxPlaca.Text.Length - 1)))
                {
                    esta = true;
                }

            }
            return esta;
            
        }

        private void txtEliminar_Click(object sender, EventArgs e)
        {
            new EventoBOL().eliminarEvento(ve.marca);
            MessageBox.Show("Borrado");
            mostrar();
            if (ve.tipo == null)
            {
                MessageBox.Show("Seleccione el evento a eliminar");
            }
            else
            {
                if (ve.tipo.Equals("Abierto"))
                {

                    new EventoBOL().eliminarEvento(ve.marca);
                    MessageBox.Show("Borrado");
                    mostrar();
                }
                else
                {
                    MessageBox.Show("Lo sentimos pero solo puede eliminar los eventos con estado abierto");
                }
            }

        }
    }
}
