﻿namespace Gestión_colisiones.GUI
{
    partial class frmJuzgado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmJuzgado));
            this.dtgPorComprobar = new System.Windows.Forms.DataGridView();
            this.txtCodigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtParte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPlaca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtOficial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtcedOficial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtVehiculo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPropietario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtCedPropie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtLugar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtFechaH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtMulta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtEstado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtNumRegistro = new System.Windows.Forms.TextBox();
            this.btnAgregarRegistro = new System.Windows.Forms.Button();
            this.lblRegistro = new System.Windows.Forms.Label();
            this.lblNombreJuez = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblfechaH = new System.Windows.Forms.Label();
            this.tHorafecha = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.miOpciones = new System.Windows.Forms.ToolStripMenuItem();
            this.multasMayoresA45000ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.completasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dtgMayoresA = new System.Windows.Forms.DataGridView();
            this.dtgCompletos = new System.Windows.Forms.DataGridView();
            this.txtcedCiud = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtNParte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtEstad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtFecH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtAnno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtMar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtColor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtTipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnRegresar = new System.Windows.Forms.Button();
            this.btnRegreso = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl45 = new System.Windows.Forms.Label();
            this.lblCompletas = new System.Windows.Forms.Label();
            this.txtCodiE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtCedOF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtOfici = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtMultaValor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPorComprobar)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgMayoresA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCompletos)).BeginInit();
            this.SuspendLayout();
            // 
            // dtgPorComprobar
            // 
            this.dtgPorComprobar.AllowUserToAddRows = false;
            this.dtgPorComprobar.AllowUserToDeleteRows = false;
            this.dtgPorComprobar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPorComprobar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtCodigo,
            this.txtParte,
            this.txtPlaca,
            this.txtOficial,
            this.txtcedOficial,
            this.txtVehiculo,
            this.txtPropietario,
            this.txtCedPropie,
            this.txtLugar,
            this.txtFechaH,
            this.txtMulta,
            this.txtEstado});
            this.dtgPorComprobar.Location = new System.Drawing.Point(46, 225);
            this.dtgPorComprobar.Name = "dtgPorComprobar";
            this.dtgPorComprobar.ReadOnly = true;
            this.dtgPorComprobar.RowHeadersWidth = 51;
            this.dtgPorComprobar.RowTemplate.Height = 24;
            this.dtgPorComprobar.Size = new System.Drawing.Size(1371, 469);
            this.dtgPorComprobar.TabIndex = 4;
            this.dtgPorComprobar.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgPorComprobar_CellClick);
            // 
            // txtCodigo
            // 
            this.txtCodigo.HeaderText = "Código";
            this.txtCodigo.MinimumWidth = 6;
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.ReadOnly = true;
            this.txtCodigo.Width = 125;
            // 
            // txtParte
            // 
            this.txtParte.HeaderText = "Num.Parte";
            this.txtParte.MinimumWidth = 6;
            this.txtParte.Name = "txtParte";
            this.txtParte.ReadOnly = true;
            this.txtParte.Width = 125;
            // 
            // txtPlaca
            // 
            this.txtPlaca.HeaderText = "Placa";
            this.txtPlaca.MinimumWidth = 6;
            this.txtPlaca.Name = "txtPlaca";
            this.txtPlaca.ReadOnly = true;
            this.txtPlaca.Width = 125;
            // 
            // txtOficial
            // 
            this.txtOficial.HeaderText = "Oficial";
            this.txtOficial.MinimumWidth = 6;
            this.txtOficial.Name = "txtOficial";
            this.txtOficial.ReadOnly = true;
            this.txtOficial.Width = 125;
            // 
            // txtcedOficial
            // 
            this.txtcedOficial.HeaderText = "Ced.Oficial";
            this.txtcedOficial.MinimumWidth = 6;
            this.txtcedOficial.Name = "txtcedOficial";
            this.txtcedOficial.ReadOnly = true;
            this.txtcedOficial.Width = 125;
            // 
            // txtVehiculo
            // 
            this.txtVehiculo.HeaderText = "Vehiculo";
            this.txtVehiculo.MinimumWidth = 6;
            this.txtVehiculo.Name = "txtVehiculo";
            this.txtVehiculo.ReadOnly = true;
            this.txtVehiculo.Width = 125;
            // 
            // txtPropietario
            // 
            this.txtPropietario.HeaderText = "Propietario";
            this.txtPropietario.MinimumWidth = 6;
            this.txtPropietario.Name = "txtPropietario";
            this.txtPropietario.ReadOnly = true;
            this.txtPropietario.Width = 125;
            // 
            // txtCedPropie
            // 
            this.txtCedPropie.HeaderText = "Ced.Propietario";
            this.txtCedPropie.MinimumWidth = 6;
            this.txtCedPropie.Name = "txtCedPropie";
            this.txtCedPropie.ReadOnly = true;
            this.txtCedPropie.Width = 125;
            // 
            // txtLugar
            // 
            this.txtLugar.HeaderText = "Lugar";
            this.txtLugar.MinimumWidth = 6;
            this.txtLugar.Name = "txtLugar";
            this.txtLugar.ReadOnly = true;
            this.txtLugar.Width = 125;
            // 
            // txtFechaH
            // 
            this.txtFechaH.HeaderText = "Fecha y hora";
            this.txtFechaH.MinimumWidth = 6;
            this.txtFechaH.Name = "txtFechaH";
            this.txtFechaH.ReadOnly = true;
            this.txtFechaH.Width = 125;
            // 
            // txtMulta
            // 
            this.txtMulta.HeaderText = "Multa";
            this.txtMulta.MinimumWidth = 6;
            this.txtMulta.Name = "txtMulta";
            this.txtMulta.ReadOnly = true;
            this.txtMulta.Width = 125;
            // 
            // txtEstado
            // 
            this.txtEstado.HeaderText = "Estado";
            this.txtEstado.MinimumWidth = 6;
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.ReadOnly = true;
            this.txtEstado.Width = 125;
            // 
            // txtNumRegistro
            // 
            this.txtNumRegistro.Location = new System.Drawing.Point(149, 199);
            this.txtNumRegistro.Name = "txtNumRegistro";
            this.txtNumRegistro.Size = new System.Drawing.Size(112, 22);
            this.txtNumRegistro.TabIndex = 5;
            this.txtNumRegistro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumRegistro_KeyPress);
            // 
            // btnAgregarRegistro
            // 
            this.btnAgregarRegistro.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btnAgregarRegistro.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAgregarRegistro.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.btnAgregarRegistro.Image = ((System.Drawing.Image)(resources.GetObject("btnAgregarRegistro.Image")));
            this.btnAgregarRegistro.Location = new System.Drawing.Point(293, 187);
            this.btnAgregarRegistro.Name = "btnAgregarRegistro";
            this.btnAgregarRegistro.Size = new System.Drawing.Size(34, 34);
            this.btnAgregarRegistro.TabIndex = 6;
            this.btnAgregarRegistro.UseVisualStyleBackColor = false;
            this.btnAgregarRegistro.Click += new System.EventHandler(this.btnAgregarRegistro_Click);
            // 
            // lblRegistro
            // 
            this.lblRegistro.AutoSize = true;
            this.lblRegistro.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistro.ForeColor = System.Drawing.SystemColors.Control;
            this.lblRegistro.Location = new System.Drawing.Point(46, 202);
            this.lblRegistro.Name = "lblRegistro";
            this.lblRegistro.Size = new System.Drawing.Size(78, 19);
            this.lblRegistro.TabIndex = 7;
            this.lblRegistro.Text = "#Registro";
            // 
            // lblNombreJuez
            // 
            this.lblNombreJuez.AutoSize = true;
            this.lblNombreJuez.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreJuez.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblNombreJuez.Location = new System.Drawing.Point(1245, 705);
            this.lblNombreJuez.Name = "lblNombreJuez";
            this.lblNombreJuez.Size = new System.Drawing.Size(44, 19);
            this.lblNombreJuez.TabIndex = 8;
            this.lblNombreJuez.Text = "Juez";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblNombre.Location = new System.Drawing.Point(1295, 707);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(60, 17);
            this.lblNombre.TabIndex = 9;
            this.lblNombre.Text = "Nombre";
            // 
            // lblfechaH
            // 
            this.lblfechaH.AutoSize = true;
            this.lblfechaH.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfechaH.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.lblfechaH.Location = new System.Drawing.Point(103, 697);
            this.lblfechaH.Name = "lblfechaH";
            this.lblfechaH.Size = new System.Drawing.Size(92, 16);
            this.lblfechaH.TabIndex = 10;
            this.lblfechaH.Text = "Fecha y hora";
            // 
            // tHorafecha
            // 
            this.tHorafecha.Enabled = true;
            this.tHorafecha.Interval = 1;
            this.tHorafecha.Tick += new System.EventHandler(this.tHorafecha_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miOpciones,
            this.salirToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1453, 28);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // miOpciones
            // 
            this.miOpciones.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.multasMayoresA45000ToolStripMenuItem,
            this.completasToolStripMenuItem});
            this.miOpciones.Name = "miOpciones";
            this.miOpciones.Size = new System.Drawing.Size(85, 24);
            this.miOpciones.Text = "Opciones";
            // 
            // multasMayoresA45000ToolStripMenuItem
            // 
            this.multasMayoresA45000ToolStripMenuItem.Name = "multasMayoresA45000ToolStripMenuItem";
            this.multasMayoresA45000ToolStripMenuItem.Size = new System.Drawing.Size(252, 26);
            this.multasMayoresA45000ToolStripMenuItem.Text = "Multas mayores a 45000";
            this.multasMayoresA45000ToolStripMenuItem.Click += new System.EventHandler(this.multasMayoresA45000ToolStripMenuItem_Click);
            // 
            // completasToolStripMenuItem
            // 
            this.completasToolStripMenuItem.Name = "completasToolStripMenuItem";
            this.completasToolStripMenuItem.Size = new System.Drawing.Size(252, 26);
            this.completasToolStripMenuItem.Text = "Completas";
            this.completasToolStripMenuItem.Click += new System.EventHandler(this.completasToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(52, 24);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // dtgMayoresA
            // 
            this.dtgMayoresA.AllowUserToAddRows = false;
            this.dtgMayoresA.AllowUserToDeleteRows = false;
            this.dtgMayoresA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgMayoresA.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtCodiE,
            this.txtCedOF,
            this.txtOfici,
            this.txtMultaValor});
            this.dtgMayoresA.Location = new System.Drawing.Point(106, 227);
            this.dtgMayoresA.Name = "dtgMayoresA";
            this.dtgMayoresA.ReadOnly = true;
            this.dtgMayoresA.RowHeadersWidth = 51;
            this.dtgMayoresA.RowTemplate.Height = 24;
            this.dtgMayoresA.Size = new System.Drawing.Size(1249, 469);
            this.dtgMayoresA.TabIndex = 12;
            // 
            // dtgCompletos
            // 
            this.dtgCompletos.AllowUserToAddRows = false;
            this.dtgCompletos.AllowUserToDeleteRows = false;
            this.dtgCompletos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgCompletos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtcedCiud,
            this.txtNParte,
            this.txtL,
            this.txtEstad,
            this.txtFecH,
            this.txtAnno,
            this.txtMar,
            this.txtColor,
            this.txtTipo});
            this.dtgCompletos.Location = new System.Drawing.Point(46, 225);
            this.dtgCompletos.Name = "dtgCompletos";
            this.dtgCompletos.ReadOnly = true;
            this.dtgCompletos.RowHeadersWidth = 51;
            this.dtgCompletos.RowTemplate.Height = 24;
            this.dtgCompletos.Size = new System.Drawing.Size(1371, 469);
            this.dtgCompletos.TabIndex = 13;
            // 
            // txtcedCiud
            // 
            this.txtcedCiud.HeaderText = "Ced.Ciudadano";
            this.txtcedCiud.MinimumWidth = 6;
            this.txtcedCiud.Name = "txtcedCiud";
            this.txtcedCiud.ReadOnly = true;
            this.txtcedCiud.Width = 125;
            // 
            // txtNParte
            // 
            this.txtNParte.HeaderText = "Num.Parte";
            this.txtNParte.MinimumWidth = 6;
            this.txtNParte.Name = "txtNParte";
            this.txtNParte.ReadOnly = true;
            this.txtNParte.Width = 125;
            // 
            // txtL
            // 
            this.txtL.HeaderText = "Lugar";
            this.txtL.MinimumWidth = 6;
            this.txtL.Name = "txtL";
            this.txtL.ReadOnly = true;
            this.txtL.Width = 125;
            // 
            // txtEstad
            // 
            this.txtEstad.HeaderText = "Estado";
            this.txtEstad.MinimumWidth = 6;
            this.txtEstad.Name = "txtEstad";
            this.txtEstad.ReadOnly = true;
            this.txtEstad.Width = 125;
            // 
            // txtFecH
            // 
            this.txtFecH.HeaderText = "Fecha y hora";
            this.txtFecH.MinimumWidth = 6;
            this.txtFecH.Name = "txtFecH";
            this.txtFecH.ReadOnly = true;
            this.txtFecH.Width = 125;
            // 
            // txtAnno
            // 
            this.txtAnno.HeaderText = "Año";
            this.txtAnno.MinimumWidth = 6;
            this.txtAnno.Name = "txtAnno";
            this.txtAnno.ReadOnly = true;
            this.txtAnno.Width = 125;
            // 
            // txtMar
            // 
            this.txtMar.HeaderText = "Marca";
            this.txtMar.MinimumWidth = 6;
            this.txtMar.Name = "txtMar";
            this.txtMar.ReadOnly = true;
            this.txtMar.Width = 125;
            // 
            // txtColor
            // 
            this.txtColor.HeaderText = "Color";
            this.txtColor.MinimumWidth = 6;
            this.txtColor.Name = "txtColor";
            this.txtColor.ReadOnly = true;
            this.txtColor.Width = 125;
            // 
            // txtTipo
            // 
            this.txtTipo.HeaderText = "Tipo";
            this.txtTipo.MinimumWidth = 6;
            this.txtTipo.Name = "txtTipo";
            this.txtTipo.ReadOnly = true;
            this.txtTipo.Width = 125;
            // 
            // btnRegresar
            // 
            this.btnRegresar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRegresar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnRegresar.Image = ((System.Drawing.Image)(resources.GetObject("btnRegresar.Image")));
            this.btnRegresar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRegresar.Location = new System.Drawing.Point(1214, 189);
            this.btnRegresar.Name = "btnRegresar";
            this.btnRegresar.Size = new System.Drawing.Size(93, 30);
            this.btnRegresar.TabIndex = 14;
            this.btnRegresar.Text = "Regresar";
            this.btnRegresar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRegresar.UseVisualStyleBackColor = true;
            this.btnRegresar.Click += new System.EventHandler(this.btnRegresar_Click);
            // 
            // btnRegreso
            // 
            this.btnRegreso.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRegreso.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnRegreso.Image = ((System.Drawing.Image)(resources.GetObject("btnRegreso.Image")));
            this.btnRegreso.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRegreso.Location = new System.Drawing.Point(1324, 189);
            this.btnRegreso.Name = "btnRegreso";
            this.btnRegreso.Size = new System.Drawing.Size(93, 30);
            this.btnRegreso.TabIndex = 15;
            this.btnRegreso.Text = "Regresar";
            this.btnRegreso.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRegreso.UseVisualStyleBackColor = true;
            this.btnRegreso.Click += new System.EventHandler(this.btnRegreso_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(671, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 53);
            this.label1.TabIndex = 16;
            this.label1.Text = "Evento";
            // 
            // lbl45
            // 
            this.lbl45.AutoSize = true;
            this.lbl45.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lbl45.Location = new System.Drawing.Point(103, 202);
            this.lbl45.Name = "lbl45";
            this.lbl45.Size = new System.Drawing.Size(246, 17);
            this.lbl45.TabIndex = 17;
            this.lbl45.Text = "Multas que exeden los 45000 colones";
            // 
            // lblCompletas
            // 
            this.lblCompletas.AutoSize = true;
            this.lblCompletas.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblCompletas.Location = new System.Drawing.Point(43, 205);
            this.lblCompletas.Name = "lblCompletas";
            this.lblCompletas.Size = new System.Drawing.Size(177, 17);
            this.lblCompletas.TabIndex = 18;
            this.lblCompletas.Text = "Multas en estado completo";
            // 
            // txtCodiE
            // 
            this.txtCodiE.HeaderText = "Codigo";
            this.txtCodiE.MinimumWidth = 6;
            this.txtCodiE.Name = "txtCodiE";
            this.txtCodiE.ReadOnly = true;
            this.txtCodiE.Width = 300;
            // 
            // txtCedOF
            // 
            this.txtCedOF.HeaderText = "Ced.oficial";
            this.txtCedOF.MinimumWidth = 6;
            this.txtCedOF.Name = "txtCedOF";
            this.txtCedOF.ReadOnly = true;
            this.txtCedOF.Width = 300;
            // 
            // txtOfici
            // 
            this.txtOfici.HeaderText = "Oficial";
            this.txtOfici.MinimumWidth = 6;
            this.txtOfici.Name = "txtOfici";
            this.txtOfici.ReadOnly = true;
            this.txtOfici.Width = 300;
            // 
            // txtMultaValor
            // 
            this.txtMultaValor.HeaderText = "Multa";
            this.txtMultaValor.MinimumWidth = 6;
            this.txtMultaValor.Name = "txtMultaValor";
            this.txtMultaValor.ReadOnly = true;
            this.txtMultaValor.Width = 300;
            // 
            // frmJuzgado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(1453, 745);
            this.Controls.Add(this.lblCompletas);
            this.Controls.Add(this.lbl45);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnRegreso);
            this.Controls.Add(this.btnRegresar);
            this.Controls.Add(this.dtgCompletos);
            this.Controls.Add(this.dtgMayoresA);
            this.Controls.Add(this.lblfechaH);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.lblNombreJuez);
            this.Controls.Add(this.lblRegistro);
            this.Controls.Add(this.btnAgregarRegistro);
            this.Controls.Add(this.txtNumRegistro);
            this.Controls.Add(this.dtgPorComprobar);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmJuzgado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmJuzgado";
            ((System.ComponentModel.ISupportInitialize)(this.dtgPorComprobar)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgMayoresA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCompletos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dtgPorComprobar;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtCodigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtParte;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPlaca;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtOficial;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtcedOficial;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtVehiculo;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPropietario;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtCedPropie;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtLugar;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtFechaH;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtMulta;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtEstado;
        private System.Windows.Forms.TextBox txtNumRegistro;
        private System.Windows.Forms.Button btnAgregarRegistro;
        private System.Windows.Forms.Label lblRegistro;
        private System.Windows.Forms.Label lblNombreJuez;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblfechaH;
        private System.Windows.Forms.Timer tHorafecha;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miOpciones;
        private System.Windows.Forms.ToolStripMenuItem multasMayoresA45000ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem completasToolStripMenuItem;
        private System.Windows.Forms.DataGridView dtgMayoresA;
        private System.Windows.Forms.DataGridView dtgCompletos;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtcedCiud;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtNParte;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtL;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtEstad;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtFecH;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtAnno;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtMar;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtColor;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtTipo;
        private System.Windows.Forms.Button btnRegresar;
        private System.Windows.Forms.Button btnRegreso;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl45;
        private System.Windows.Forms.Label lblCompletas;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtCodiE;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtCedOF;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtOfici;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtMultaValor;
    }
}