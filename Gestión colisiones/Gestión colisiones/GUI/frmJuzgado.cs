﻿using Gestión_colisiones.BOL;
using Gestión_colisiones.DAL;
using Gestión_colisiones.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gestión_colisiones.GUI
{
    public partial class frmJuzgado : Form
    {
        FrmLogin frm = new FrmLogin();
        int c = 0;
        EventoE ee = new EventoE();
        EventoBOL eb = new EventoBOL();
        public frmJuzgado(string nombre, int cedula )
        {
            InitializeComponent();
            dtgMayoresA.Visible = false;
            dtgCompletos.Visible = false;
            btnRegresar.Visible = false;
            btnRegreso.Visible = false;
            lbl45.Visible = false;
            lblCompletas.Visible = false;
            c = cedula;
            lblNombre.Text = nombre;
            mostrar();
        }

        private void lblCerrarSesión_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dtgPorComprobar_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            {
                DataGridViewRow row = dtgPorComprobar.Rows[e.RowIndex];
                ee.codigo = row.Cells[0].Value.ToString();
            }
        }
        public void mostrar()
        {
            dtgPorComprobar.Rows.Clear();
            foreach (EventoE ev in new EventoDAL().LLenarDataG())
            {
                if(ev.estado.Equals("Por Aprobar"))
                dtgPorComprobar.Rows.Add(ev.codigo,ev.numparte, ev.placa,ev.nombreOficial, ev.cedOficial, ev.vehiculo, ev.nombreC, ev.cedCiuda, ev.provincia, ev.fecha, ev.multa, ev.estado);
            }

        }

        private void txtNumRegistro_KeyPress(object sender, KeyPressEventArgs e)
        {
            soloNumeros(e);
        }
        private void soloNumeros(KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
            {
                e.Handled = false;

            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;

            }
            else
            {

                e.Handled = true;
            }


        }

        private void btnAgregarRegistro_Click(object sender, EventArgs e)
        {

            if (txtNumRegistro.Text.Trim().Equals(""))
            {
                MessageBox.Show("Lo siento debe ingresar un numero de parte");
            }
            else
            {
                ee.registro = Convert.ToInt32(txtNumRegistro.Text);
                ee.estado = "Completo";
                ee.cedJuez = c;
                ee.nombreJuez = lblNombre.Text;
                ee.fecha = DateTime.Now;
                eb.editarEventoJ(ee);
                mostrar();
            }

        }

        private void tHorafecha_Tick(object sender, EventArgs e)
        {
            lblfechaH.Text = DateTime.Now.ToString();
        }

        
        private void mostrar2()
        {

            dtgMayoresA.Rows.Clear();
            foreach (EventoE e in new EventoDAL().LLenarDataG())
            {
                if (e.multa > 45000)
                {
                    dtgMayoresA.Rows.Add(e.codigo,e.cedOficial, e.nombreOficial, e.multa);
                }
            }

        }
        private void mostrar3()
        {

            dtgCompletos.Rows.Clear();
            foreach (EventoE e in new EventoDAL().LLenarDataG())
            {
                if (e.estado.Equals("Completo"))
                {
                    foreach (VehiculoE ve in new VehiculoDAL().LLenarDataG())
                    {
                        if (e.placa == ve.placa)
                        {

                            dtgCompletos.Rows.Add(e.cedCiuda, e.numparte, e.provincia, e.estado, e.fecha, ve.anno, ve.marca, ve.color, ve.tipo);
                        }
                    }   
                }
            }

        }

        private void multasMayoresA45000ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dtgMayoresA.Visible = true;
            dtgPorComprobar.Visible = false;
            dtgCompletos.Visible = false;
            lblRegistro.Visible = false;
            txtNumRegistro.Visible = false;
            btnAgregarRegistro.Visible = false;
            btnRegresar.Visible = true;
            lbl45.Visible = true;
            lblCompletas.Visible = false;
            mostrar2();

        }

        private void completasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dtgCompletos.Visible = true;
            dtgMayoresA.Visible = false;
            dtgPorComprobar.Visible = false;
            lblRegistro.Visible = false;
            txtNumRegistro.Visible = false;
            btnAgregarRegistro.Visible = false;
            btnRegreso.Visible = true;
            lbl45.Visible = false;
            lblCompletas.Visible = true;
            mostrar3();
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            dtgMayoresA.Visible = false;
            dtgPorComprobar.Visible = true;
            dtgCompletos.Visible = false;
            lblRegistro.Visible = true;
            txtNumRegistro.Visible = true;
            btnAgregarRegistro.Visible = true;
            btnRegresar.Visible = false;
            lbl45.Visible = false;
            lblCompletas.Visible = false;
            mostrar();
        }

        private void btnRegreso_Click(object sender, EventArgs e)
        {
            dtgMayoresA.Visible = false;
            dtgPorComprobar.Visible = true;
            dtgCompletos.Visible = false;
            lblRegistro.Visible = true;
            txtNumRegistro.Visible = true;
            btnAgregarRegistro.Visible = true;
            btnRegreso.Visible = false;
            lblCompletas.Visible = false;
            lbl45.Visible = false;
            mostrar();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Close();
            frm.Show();
            Hide();
        }
    }
}
